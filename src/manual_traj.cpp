#include "ros/ros.h"
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Empty.h>
#include <nav_msgs/Odometry.h>
#include <spektrum/MikrokopterStatus.h>
#include <ca_common/math.h>
#include <cstdlib>
#include <algorithm>

using namespace CA;

ros::Publisher path_pub;
ros::Publisher reset_pub;

double heading_fb;
bool manual_mode = true;

CA::Vector3D current_position;
CA::Trajectory path;

void pathCallback(const ca_common::Trajectory::ConstPtr& msg)
{
    path.fromMsg(*msg);
    std::vector<State, Eigen::aligned_allocator<Vector3D> > t = path.getLinear();

    Eigen::AngleAxis<double> Rwl = Eigen::AngleAxis<double>(heading_fb, Eigen::Vector3d(0,0,1));
    Eigen::Isometry3d Twl(Rwl);
    Twl.translation() = current_position;


    for(unsigned int i=0; i<t.size(); i++) {
        //path.t[i].pose.position_m += current_position;

        t[i].pose.position_m = (Twl * t[i].pose.position_m ).eval();
        //path.t[i].pose.orientation_rad(2) += heading_fb;
        t[i].pose.orientation_rad(2) = heading_fb;

        if(t[i].pose.orientation_rad(2) > M_PI)
            t[i].pose.orientation_rad(2) -= 2*M_PI;
        if(t[i].pose.orientation_rad(2) < -M_PI)
            t[i].pose.orientation_rad(2) += 2*M_PI;

        t[i].rates.velocity_mps = (Rwl * t[i].rates.velocity_mps ).eval();

    }

    path.setLinear(t);

    ROS_DEBUG_STREAM("Got path");
}

void orientationFbCallback(const geometry_msgs::Quaternion::ConstPtr& msg)
{

    ROS_DEBUG_STREAM("Got orientation feedback");
}

void poseCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
    current_position[0] = msg->pose.pose.position.x;
    current_position[1] = msg->pose.pose.position.y;
    current_position[2] = msg->pose.pose.position.z;
    CA::Vector3D euler;
    euler = CA::quatToEuler(CA::msgc(msg->pose.pose.orientation));
    heading_fb = euler[2];

}

void rcCallback(const spektrum::MikrokopterStatus::ConstPtr& msg)
{
    if(msg->isAutonomous) {
        // If we are switching modes, send the setpoints
        if(manual_mode) {
            std_msgs::Empty rst;

            ca_common::Trajectory t;
            t = path.toMsg();
            t.header.frame_id = "/world";

            reset_pub.publish(rst);
            path_pub.publish(t);

            ROS_INFO_STREAM("Sent path");
        }
        manual_mode = false;
    } else {
        manual_mode = true;
    }


    ROS_DEBUG_STREAM("Got RC packet");
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "manual_traj");

    ros::NodeHandle n;

    path_pub = n.advertise<ca_common::Trajectory>("/trajectory_control/path", 100);
    reset_pub = n.advertise<std_msgs::Empty>("/hav_control/reset", 1);

    ros::Subscriber path_sub = n.subscribe<ca_common::Trajectory>("/trajectory_gen/path", 1, pathCallback);
    ros::Subscriber rc_sub = n.subscribe<spektrum::MikrokopterStatus>("/spektrum/status",1,rcCallback);
    ros::Subscriber pose_fb_sub = n.subscribe<nav_msgs::Odometry>("/trajectory_control/odometry", 1, poseCallback);

    ros::spin();

    return 0;
}


