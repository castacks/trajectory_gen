#include "ros/ros.h"
#include <cstdlib>
#include <algorithm>
#include <ca_common/Trajectory.h>
#include <ca_common/math.h>
#include <spektrum/MikrokopterStatus.h>
#include <ca_common/MissionWaypoint.h>
#include <ca_common/Mission.h>
#include <ca_common/MissionCommand.h>

using namespace CA;
CA::Trajectory path;

Vector3D currentXYZ;
Vector3D hoverAtXYZ;
double last_heading,hoverHeading;
nav_msgs::Odometry startOdom;
bool fistTime = false;
bool started = false;
bool gotPose = false;

void getPose(const nav_msgs::OdometryConstPtr &msg)
{
    //ROS_INFO("pose received sq trajectory gen");
    startOdom = (*msg);
    currentXYZ = msgc((*msg).pose.pose.position);
    QuatD orientation = CA::msgc((*msg).pose.pose.orientation);
    Vector3D attitude;
    attitude = CA::quatToEuler(orientation);
    last_heading = attitude[2];
    gotPose = true;
}

void getStart(const spektrum::MikrokopterStatus::ConstPtr &msg)
{
    if(msg->isAutonomous && started == false) {
        if(!gotPose) {
            ROS_ERROR_STREAM("never got pose!");
            return;
        }

        if ((msg->header.stamp - startOdom.header.stamp).toSec() > 0.5) {
            ROS_ERROR_STREAM("odometry msg too old!");
            return;
        }
        hoverAtXYZ=currentXYZ;
        hoverHeading=last_heading;

        fistTime = true;
        started = true;
        ROS_WARN_STREAM("Traj gen starts at  hold hover!" <<hoverAtXYZ);

    } else if(!msg->isAutonomous) {
        started = false;
    }
}
int main(int argc, char **argv)
{
    ros::init(argc, argv, "trajectory_gen");

    ros::NodeHandle n;

    ros::Publisher path_pub = n.advertise<ca_common::Trajectory>("/trajectory_gen/path", 100);

    double loopRate;
    int nodeCount;
    double speed,speedMax;

    double A,Z,countMax,speedMin;

    n.param("/trajectory_gen/loopRate", loopRate, 4.0);
    n.param("/trajectory_gen/nodeCount", nodeCount, 20);
    n.param("/trajectory_gen/speedMax", speedMax, 1.0);
    n.param("/trajectory_gen/count", countMax, 1.0);
    n.param("/trajectory_gen/speedMin", speedMin, 1.0);

    n.param("/trajectory_gen/amplitude", A, 4.0);
    n.param("/trajectory_gen/amplitudeZ", Z, 1.0);
    ros::Subscriber subPose   = n.subscribe("odom",10,&getPose);
    ros::Subscriber subStart   = n.subscribe("/spektrum/status",1,&getStart);
    hoverAtXYZ[0]=hoverAtXYZ[1]=hoverAtXYZ[2]=0;
    currentXYZ[0]=currentXYZ[1]=currentXYZ[2]=0;

    speed = 0.1;
    std::vector<State, Eigen::aligned_allocator<Vector3D> > sl;


    ros::Rate loop_rate(loopRate);
    ca_common::Trajectory t;
    t.header.seq=1;

    while (ros::ok()) {
        if(fistTime)
        {
            sl.clear();
            for(int count=0; count<countMax; count++) {

//Round forward
                for(int n=0; n<nodeCount; n++) {
                    speed = speed *0.7+ 0.3 *((speedMax-speedMin) * ((double)count)/((double)(countMax-1)) + speedMin);
                    CA::State c;

                    double t = 8.0*((double)n/(nodeCount-1));

                    double x = -2*A * (std::cos(M_PI/4 * t) - 1);
                    double y = A*std::sin(M_PI/2 * t);
                    double z = -Z*std::sin(M_PI*t);

                    c.pose.position_m[0] = x +hoverAtXYZ[0];
                    c.pose.position_m[1] = y +hoverAtXYZ[1];
                    c.pose.position_m[2] = z +hoverAtXYZ[2];

                    double vy =  A*M_PI/2*std::cos(M_PI/2.0 * t);
                    double vx =  A*M_PI/2*std::sin(M_PI/4.0 * t);
                    double vz =  -Z*M_PI*std::cos(M_PI * t);
                    double heading = std::atan2(vy,vx);
                    //double heading = 0;
                    c.pose.orientation_rad[2] = heading;

                    c.rates.velocity_mps[0] = vx;
                    c.rates.velocity_mps[1] = vy;
                    c.rates.velocity_mps[2] = vz;

                    c.rates.velocity_mps.normalize();
                    c.rates.velocity_mps *= speed;

                    sl.push_back(c);
                }



            }
            path.setLinear(sl);
            t = path.toMsg();
            t.header.stamp = ros::Time::now();
            t.header.seq++;
            t.header.frame_id = "world";
            path_pub.publish(t);
            fistTime=false;
        }
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}


