#include "ros/ros.h"
#include <cstdlib>
#include <algorithm>
#include <ca_common/Trajectory.h>
#include <ca_common/math.h>
#include <spektrum/MikrokopterStatus.h>
#include <ca_common/MissionWaypoint.h>
#include <ca_common/Mission.h>
#include <ca_common/MissionCommand.h>

using namespace CA;

CA::Trajectory path;

Vector3D currentXYZ;
Vector3D hoverAtXYZ;
double last_heading,hoverHeading;
nav_msgs::Odometry startOdom;
bool fistTime = false;
bool started = false;
bool gotPose = false;

void getPose(const nav_msgs::OdometryConstPtr &msg)
{
    //ROS_INFO("pose received sq trajectory gen");
    startOdom = (*msg);
    currentXYZ = msgc((*msg).pose.pose.position);
    QuatD orientation = CA::msgc((*msg).pose.pose.orientation);
    Vector3D attitude;
    attitude = CA::quatToEuler(orientation);
    last_heading = attitude[2];
    gotPose = true;
}

void getStart(const spektrum::MikrokopterStatus::ConstPtr &msg)
{
    if(msg->isAutonomous && started == false) {
        if(!gotPose) {
            ROS_ERROR_STREAM("never got pose!");
            return;
        }

        if ((msg->header.stamp - startOdom.header.stamp).toSec() > 0.5) {
            ROS_ERROR_STREAM("odometry msg too old!");
            return;
        }
        hoverAtXYZ=currentXYZ;
        hoverHeading=last_heading;

        fistTime = true;
        started = true;
        ROS_WARN_STREAM("Traj gen starts at  hold hover!" <<hoverAtXYZ);

    } else if(!msg->isAutonomous) {
        started = false;
    }
}
int main(int argc, char **argv)
{
    ros::init(argc, argv, "trajectory_gen");

    ros::NodeHandle n;

    ros::Publisher path_pub = n.advertise<ca_common::Trajectory>("/trajectory_gen/path", 100);

    double loopRate;
    double nodeSpacing;
    double speed;

    double x,y,z;

    n.param("/trajectory_gen/loopRate", loopRate, 4.0);
    n.param("/trajectory_gen/nodeSpacing", nodeSpacing, 1.0);
    n.param("/trajectory_gen/speed", speed, 1.0);

    n.param("/trajectory_gen/hover_x", x, 0.0);
    n.param("/trajectory_gen/hover_y", y, 0.0);
    n.param("/trajectory_gen/hover_z", z, 0.0);

    ros::Subscriber subPose   = n.subscribe("odom",10,&getPose);
    ros::Subscriber subStart   = n.subscribe("/spektrum/status",1,&getStart);
    hoverAtXYZ[0]=hoverAtXYZ[1]=hoverAtXYZ[2]=0;
    currentXYZ[0]=currentXYZ[1]=currentXYZ[2]=0;

    std::vector<State, Eigen::aligned_allocator<Vector3D> > sl;



    ros::Rate loop_rate(loopRate);
    ca_common::Trajectory t;
    t.header.seq=1;

    while (ros::ok()) {
        if(fistTime)
        {
            sl.clear();
            CA::State c;
            c.pose.position_m[0] = x +hoverAtXYZ[0];
            c.pose.position_m[1] = y +hoverAtXYZ[1];
            c.pose.position_m[2] = z +hoverAtXYZ[2];

            c.pose.orientation_rad[2] = 0.0;

            c.rates.velocity_mps[0] = 0.0;
            c.rates.velocity_mps[1] = 0.0;
            c.rates.velocity_mps[2] = 0.0;

            sl.push_back(c);
            path.setLinear(sl);

            t = path.toMsg();
            t.header.stamp = ros::Time::now();
            t.header.seq++;
            t.header.frame_id = "world";
            path_pub.publish(t);
            fistTime=false;
        }
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}


